package com.example.comoisitekeydemo.models;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
public class EmployeeIdentity implements Serializable {

    @NotNull
    private String employeeId;

    @NotNull
    private String companyId;

}
