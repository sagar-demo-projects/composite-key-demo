package com.example.comoisitekeydemo.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@NoArgsConstructor
public class Employee {

    @NaturalId
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @EmbeddedId
    private EmployeeIdentity employeeIdentity;

    @NotNull
    private String email;

    @Column(name = "phone_number", unique = true)
    private String phoneNumber;


}
