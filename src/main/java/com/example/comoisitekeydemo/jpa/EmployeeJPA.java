package com.example.comoisitekeydemo.jpa;

import com.example.comoisitekeydemo.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeJPA extends JpaRepository<Employee, Long> {
}
