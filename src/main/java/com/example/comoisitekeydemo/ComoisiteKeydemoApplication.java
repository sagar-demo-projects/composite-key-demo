package com.example.comoisitekeydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComoisiteKeydemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ComoisiteKeydemoApplication.class, args);
    }

}
